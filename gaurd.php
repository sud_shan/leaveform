<?php


session_start();
require_once 'sessiontimout.php';
if(isset($_SESSION['user'])&& $_SESSION['role']==='G')
{
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Leave Form Management</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 20px;
        padding-bottom: 60px;
      }

      /* Custom container */
      .container {
        margin: 0 auto;
        max-width: 1000px;
      }
      .container > hr {
        margin: 60px 0;
      }

      /* Main marketing message and sign up button */
      .jumbotron {
        margin: 80px 0;
        text-align: center;
      }
      .jumbotron h1 {
        font-size: 100px;
        line-height: 1;
      }
      .jumbotron .lead {
        font-size: 24px;
        line-height: 1.25;
      }
      .jumbotron .btn {
        font-size: 21px;
        padding: 14px 24px;
      }

      /* Supporting marketing content */
      .marketing {
        margin: 60px 0;
      }
      .marketing p + h4 {
        margin-top: 28px;
      }


      /* Customize the navbar links to be fill the entire space of the .navbar */
      .navbar .navbar-inner {
        padding: 0;
      }
      .navbar .nav {
        margin: 0;
        display: table;
        width: 100%;
      }
      .navbar .nav li {
        display: table-cell;
        width: 1%;
        float: none;
      }
      .navbar .nav li a {
        font-weight: bold;
        text-align: center;
        border-left: 1px solid rgba(255,255,255,.75);
        border-right: 1px solid rgba(0,0,0,.1);
      }
      .navbar .nav li:first-child a {
        border-left: 0;
        border-radius: 3px 0 0 3px;
      }
      .navbar .nav li:last-child a {
        border-right: 0;
        border-radius: 0 3px 3px 0;
      }
    </style>
    <link href="css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">
                                   <link rel="shortcut icon" href="ico/favicon.png">
  </head>
<?php
include  'gaurd_header.php';
include  'timefile.php';
?>
  <body>

    <div class="container">

      <div class="masthead">
        </div>
<form name="form1" method="POST" action="warden_write.php">
		<table class="table table-bordered">
		<tr><th>Name<th>Registration number<th>Block<th>Room Number<Th>Out Time<th>In Time<th>Visiting Address<th>Student Mobile<th>Father's Mobile<th>Status<th>Image<th>Check
		<?php
		require_once 'credentials.php';

		$today = date("Y-m-d");
	
		
$x='0000-00-00 00:00:00';
$sql1="Select * from leave_form where  gate_in ='$x' and status='Approved'order by fromdate" ;
$res=mysqli_query($con,$sql1)or die("cannot get");

  

while($r=mysqli_fetch_array($res))
{
if($r['gate_in']==0)
{
$x=$r['regno'];
$qry = "select image from register where registerno='$x' ";
$res1 = mysqli_query($con,$qry) or die(mysqli_error());
$row = mysqli_fetch_array($res1);
//date_default_timezone_set('Asia/Calcutta');
//echo date("Y-m-d")."<br>".date('h:i:s', time());
if($r['status']=="Approved")
{
			$today = date("Y-m-d");

$time = date('H:i:s', time());
//echo $date;
//if($r['todate']<=$today && $time > $r['intime'] )

if($r['todate']<$today)
{
ECHO "<TR style='background-color:orange;'>";
//echo "late";
}
else if ($r['todate']==$today && $time > $admintime )
{
ECHO "<TR style='background-color:orange;'>";
}
else
{
ECHO "<TR class='success'>";
}
//ECHO "<TR class='success'>";
echo "<TD>".$r['name'];
echo "<TD>".$r['regno'];

echo "<TD>".$r['block'];
echo "<TD>".$r['roomno'];

echo "<TD>".$r['fromdate']." ".$r['outtime'];
echo "<TD>".$r['todate']." ".$r['intime'];
echo "<TD>".$r['visiting_address'];
echo "<TD>".$r['student_mobile'];
echo "<TD>".$r['father_mobile'];
echo "<TD>".$r['status'];
$x1=$row['image'];
?>
<TD><img src="data:image/jpeg;base64,<?php echo base64_encode( $x1 ); ?>" height="20" width="50" />

<?php
echo "<TD>";
if($r['gate_out']==0)
{
?>
<a href="gaurd_out.php?id=<?php echo $r['id']; ?>&& status=out">
<input type="button" name="out" class="btn" value="Out">
</a>
<?php
}
else
{
?>
<a href="gaurd_out.php?id=<?php echo $r['id']; ?>&& status=in">
<input type="button" name="In" class="btn" value="In">
</a>


<?php
}

}
if($r['status']=="Pending")
{
ECHO "<TR class='info'>";
echo "<TD>".$r['name'];
echo "<TD>".$r['regno'];

echo "<TD>".$r['block'];
echo "<TD>".$r['roomno'];

echo "<TD>".$r['fromdate']." ".$r['outtime'];
echo "<TD>".$r['todate']." ".$r['intime'];
echo "<TD>".$r['visiting_address'];
echo "<TD>".$r['student_mobile'];
echo "<TD>".$r['father_mobile'];
echo "<TD>".$r['status'];
$x1=$row['image'];

?>
<TD><img src="data:image/jpeg;base64,<?php echo base64_encode( $x1 ); ?>" height="20" width="50" />
<td>
<?php
//
 }
if($r['status']=="Declined")
{
ECHO "<TR class='error'>";
echo "<TD>".$r['name'];
echo "<TD>".$r['regno'];

echo "<TD>".$r['block'];
echo "<TD>".$r['roomno'];

echo "<TD>".$r['fromdate']." ".$r['outtime'];
echo "<TD>".$r['todate']." ".$r['intime'];
echo "<TD>".$r['visiting_address'];
echo "<TD>".$r['student_mobile'];
echo "<TD>".$r['father_mobile'];
echo "<TD>".$r['status'];
$x1=$row['image'];

?>
<TD><img src="data:image/jpeg;base64,<?php echo base64_encode( $x1 ); ?>" height="20" width="50" />
<td>
<?php
}
?>

<?php

}
}
		
?>
		
		</table>
		
		
      <!-- Jumbotron -->
      
      <!-- Example row of columns -->
      
      
    </div> <!-- /container -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap-transition.js"></script>
    <script src="js/bootstrap-alert.js"></script>
    <script src="js/bootstrap-modal.js"></script>
    <script src="js/bootstrap-dropdown.js"></script>
    <script src="js/bootstrap-scrollspy.js"></script>
    <script src="js/bootstrap-tab.js"></script>
    <script src="js/bootstrap-tooltip.js"></script>
    <script src="js/bootstrap-popover.js"></script>
    <script src="js/bootstrap-button.js"></script>
    <script src="js/bootstrap-collapse.js"></script>
    <script src="js/bootstrap-carousel.js"></script>
    <script src="js/bootstrap-typeahead.js"></script>

  </body>
</html>
<?php
}
else
echo "<script type="."text/javascript".">location.href = 'index.php?id=kindly login before continuing';</script>";

?>
