<?php
	session_start();
	
	require_once 'credentials.php';
	$user=$_SESSION['user'];
?>
		
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>View Status</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
      }

      .form-signin {
        max-width: 300px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin input[type="text"],
      .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }

    </style>
    <link href="css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">
                                   <link rel="shortcut icon" href="ico/favicon.png">
  </head>
<?php
include  'header.php';
?>

  <body>
 <div class="container">

      <div class="masthead">
        <form class="form-horizontal" method="post" align="center">
<table class="table table-bordered" >
<tr><th>Name<th>Registration number<th>Block<th>Room Number<Th>Out Time<th>In Time<th>Visiting Address<th>Student Mobile<th>Father's Mobile<th>Status<Th>Approved/Declined By<th>Reason
		
<?php
$sql1="select * from leave_form where regno='$user' order by fromdate" ;
$res=mysqli_query($con,$sql1)or die("cannot get");
while($r=mysqli_fetch_array($res))
{
if($r['status']=="Approved")
{
ECHO "<TR class='success'>";
echo "<TD>".$r['name'];
echo "<TD>".$r['regno'];

echo "<TD>".$r['block'];
echo "<TD>".$r['roomno'];

echo "<TD>".$r['fromdate']." ".$r['outtime'];
echo "<TD>".$r['todate']." ".$r['intime'];
echo "<TD>".$r['visiting_address'];
echo "<TD>".$r['student_mobile'];
echo "<TD>".$r['father_mobile'];
echo "<TD>".$r['status'];
echo "<TD>".$r['approvedby'];
echo "<TD>".$r['reason'];

}
if($r['status']=="Pending")
{
ECHO "<TR class='info'>";
echo "<TD>".$r['name'];
echo "<TD>".$r['regno'];

echo "<TD>".$r['block'];
echo "<TD>".$r['roomno'];
echo "<TD>".$r['fromdate']." ".$r['outtime'];
echo "<TD>".$r['todate']." ".$r['intime'];

echo "<TD>".$r['visiting_address'];
echo "<TD>".$r['student_mobile'];
echo "<TD>".$r['father_mobile'];
echo "<TD>".$r['status'];echo "<TD>".$r['approvedby'];
echo "<TD>".$r['reason'];

}
if($r['status']=="Declined")
{
ECHO "<TR class='error'>";
echo "<TD>".$r['name'];
echo "<TD>".$r['regno'];

echo "<TD>".$r['block'];
echo "<TD>".$r['roomno'];
echo "<TD>".$r['fromdate']." ".$r['outtime'];
echo "<TD>".$r['todate']." ".$r['intime'];

echo "<TD>".$r['visiting_address'];
echo "<TD>".$r['student_mobile'];
echo "<TD>".$r['father_mobile'];
echo "<TD>".$r['status'];
echo "<TD>".$r['approvedby'];
echo "<TD>".$r['reason'];
}


}
?>




</table>

</form>
</div>
</div>
<div id="footer">
</div>
<script src="js/jquery.js"></script>
    <script src="js/bootstrap-transition.js"></script>
    <script src="js/bootstrap-alert.js"></script>
    <script src="js/bootstrap-modal.js"></script>
    <script src="js/bootstrap-dropdown.js"></script>
    <script src="js/bootstrap-scrollspy.js"></script>
    <script src="js/bootstrap-tab.js"></script>
    <script src="js/bootstrap-tooltip.js"></script>
    <script src="js/bootstrap-popover.js"></script>
    <script src="js/bootstrap-button.js"></script>
    <script src="js/bootstrap-collapse.js"></script>
    <script src="js/bootstrap-carousel.js"></script>
    <script src="js/bootstrap-typeahead.js"></script>

  </body>
</html>


?>
